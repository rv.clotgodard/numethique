---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
## Sauvegarde


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Bacula.png">
    </div>
    <div>
      <h2>Bacula</h2>
      <p>Logiciel de sauvegarde et de restauration construit comme un système de sauvegarde natif basé sur Linux.</p>
      <div>
        <a href="https://framalibre.org/notices/bacula.html">Vers la notice Framalibre</a>
        <a href="https://www.bacula.org/">Vers le site</a>
      </div>
    </div>
  </article> 